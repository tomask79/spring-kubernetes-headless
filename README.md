## Kubernetes and howto directly reach the POD ##

Most of the time when talking to your application [pods](https://kubernetes.io/docs/concepts/workloads/pods/pod/) you want Kubernetes to load-balance your request to one of your replicas. Well in some special cases you may want to reach the pods directly like let's say to directly call pod1 from pod0 when having two replicas. This model is used for example by [Hazelcast Kubernetes Discovery Plugin](https://github.com/hazelcast/hazelcast-kubernetes) when forming the hazalcast cluster.
But this can also be useful when having some mesh of microservices hence it's worth of the test!

### Spring MVC POD application for direct reaching ###

As the test case let's create again very simple REST controller showing the name of the host POD:

```java
@RestController
public class Controller {

    @GetMapping("/invoke")
    public String invokePodController() {
        final String hostName = System.getenv("HOSTNAME");
        return "Hello, I'm the POD with name "+hostName+" \n";
    }
}
```

### Kubernetes headless service and StatefulSet ###

[Kubernetes headless-service](https://kubernetes.io/docs/concepts/services-networking/service/#headless-services) is the first prerequisity for invoking the pods directly.
Why? Because with headless-service you won't get just one internal DNS record as with the traditional service which serves as LB proxy to your pods.
But with headless-service kubernetes will spinup DNS subdomain for every POD replica in the format:    

```
<StatefulSet-Name>-<Ordinal-Index>.<ServiceName>
```
To use this format you need to keep the unique stable network ID of your PODS, so you have to use [statefulsets](https://kubernetes.io/docs/concepts/workloads/controllers/statefulset/).
Now for example let's have headless-service with name "headless" and StatefulSet "web" with two replicas. With mentioned setup **POD web-0 can directly talk to
web-1 POD using the server name web-1.headless and vice versa. **     

Let's test if it's true, first headless-service:    


```yml
apiVersion: v1
kind: Service
metadata:
  name: headless
  labels:
    app: headless
spec:
  ports:
  - port: 8080
    name: web
  clusterIP: None
  selector:
    app: headless
```

Now StatefulSet:    

```yml
apiVersion: apps/v1
kind: StatefulSet
metadata:
  name: web
  labels:
    app: headless
spec:
  serviceName: "headless"
  selector:
    matchLabels:
      app: headless
  replicas: 2
  template:
    metadata:
      labels:
        app: headless
    spec:
      containers:
      - name: headless-test
        image: headless-service-test:0.0.1-SNAPSHOT
        ports:
        - containerPort: 8080
          name: web
```

### Packaging the demo and installing the K8s manifests ###

* Switch to the main maven directory after cloning this repo
* Switch your docker daemon into minikube

```
tomask79:~ tomask79$ eval $(minikube docker-env)
```

* Compile, package and build the docker image headless-service-test:0.0.1-SNAPSHOT

```
tomask79:demo tomask79$ mvn clean install
```

* Install previously mentioned k8s manifests, headless-service "headless" and statefulset "web"

```
tomask79:kubernetes tomask79$ ls -l
total 8
-rw-r--r--  1 tomask79  staff  601 Mar 23 20:53 statefullset.yaml
tomask79:kubernetes tomask79$ pwd
/Users/tomask79/workspace/directpod/demo/kubernetes
tomask79:kubernetes tomask79$ kubectl apply -f statefullset.yaml 
service/headless unchanged
statefulset.apps/web created
```

* This is the desired output after installing the K8s manifests:

```
tomask79:kubernetes tomask79$ kubectl get statefulsets
NAME   READY   AGE
web    2/2     6m59s
tomask79:kubernetes tomask79$ kubectl get pods
NAME                              READY   STATUS    RESTARTS   AGE
web-0                             1/1     Running   0          7m5s
web-1                             1/1     Running   0          7m3s
tomask79:kubernetes tomask79$ kubectl get services
NAME         TYPE        CLUSTER-IP   EXTERNAL-IP   PORT(S)    AGE
headless     ClusterIP   None         <none>        8080/TCP   24h
kubernetes   ClusterIP   10.96.0.1    <none>        443/TCP    56d
```

### Testing the demo ###

Spinup work done, everything ready, now the fun begins...

* Open the bash inside of the web-0 POD

```
tomask79:kubernetes tomask79$ kubectl exec -it web-0 /bin/bash
root@web-0:/# 
```

* Now notice, you can ping the second POD directly!

```
root@web-0:/# ping web-1.headless
PING web-1.headless.default.svc.cluster.local (172.17.0.19) 56(84) bytes of data.
64 bytes from web-1.headless.default.svc.cluster.local (172.17.0.19): icmp_seq=1 ttl=64 time=0.207 ms
64 bytes from web-1.headless.default.svc.cluster.local (172.17.0.19): icmp_seq=2 ttl=64 time=0.090 ms
64 bytes from web-1.headless.default.svc.cluster.local (172.17.0.19): icmp_seq=3 ttl=64 time=0.051 ms
64 bytes from web-1.headless.default.svc.cluster.local (172.17.0.19): icmp_seq=4 ttl=64 time=0.049 ms
64 bytes from web-1.headless.default.svc.cluster.local (172.17.0.19): icmp_seq=5 ttl=64 time=0.052 ms
64 bytes from web-1.headless.default.svc.cluster.local (172.17.0.19): icmp_seq=6 ttl=64 time=0.089 ms
```

* If we can ping the POD directly then let's call the Spring REST endpoint:

```
root@web-0:/# curl http://web-1.headless:8080/invoke
Hello, I'm the POD with name web-1 
root@web-0:/# 
```

* Do the same test vice-versa, start the bash inside of the web-1 POD

```
tomask79:kubernetes tomask79$ kubectl exec -it web-1 /bin/bash
root@web-1:/# 
```

* Ping the web-0 POD this time

```
root@web-1:/# ping web-0.headless
PING web-0.headless.default.svc.cluster.local (172.17.0.14) 56(84) bytes of data.
64 bytes from web-0.headless.default.svc.cluster.local (172.17.0.14): icmp_seq=1 ttl=64 time=0.066 ms
64 bytes from web-0.headless.default.svc.cluster.local (172.17.0.14): icmp_seq=2 ttl=64 time=0.050 ms
```

* And if you can ping the web-0 POD then again invoke the Spring REST endpoint at web-0 POD

```
root@web-1:/# curl http://web-0.headless:8080/invoke
Hello, I'm the POD with name web-0 
```

Best Regards

Tomas