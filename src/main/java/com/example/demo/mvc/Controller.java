package com.example.demo.mvc;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controller {

    @GetMapping("/invoke")
    public String invokePodController() {
        final String hostName = System.getenv("HOSTNAME");
        return "Hello, I'm the POD with name "+hostName+" \n";
    }
}
